package uk.ac.york.es.thrift.examples.streams.subscribers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.es.thrift.streams.subscribers.RawThriftStreamSubscriber;
import uk.ac.york.thrift.es.api.Filter;
import uk.ac.york.thrift.es.api.Notification;
import uk.ac.york.thrift.es.api.Publisher;
import uk.ac.york.thrift.es.api.Subscriber;

/**
 * Bidirectional RPC example, client side. Based on the example in:
 *
 *   http://joelpm.com/2009/04/03/thrift-bidirectional-async-rpc.html
 *
 * @author Antonio García-Domínguez
 */
public class SubscriberExample {
	private static Logger LOGGER = LoggerFactory.getLogger(SubscriberExample.class);

	public static void main(String[] args) throws IOException, TException {
		final RawThriftStreamSubscriber<Subscriber.Iface, Publisher.Iface, Publisher.Client> bidiClient =
			new RawThriftStreamSubscriber<Subscriber.Iface, Publisher.Iface, Publisher.Client>(
				"localhost", 2552,
				new Publisher.Client.Factory(),
				Publisher.Iface.class,
				new Subscriber.Processor<Subscriber.Iface>(
						new Subscriber.Iface() {
							@Override
							public void notify(Notification n) throws TException {
								LOGGER.info("Received notification: {}", n.message);
							}
						})
		);

		final Publisher.Iface client = bidiClient.getPublisherClient();
		try {
			final BufferedReader rIn = new BufferedReader(new InputStreamReader(System.in));
			String line;
			do {
				System.out.print("Type a pattern and hit ENTER (empty line to exit): ");
				line = rIn.readLine();
				if (!"".equals(line)) {
					client.subscribe(new Filter(line));
				}
			} while (!"".equals(line));
		} finally {
			bidiClient.stop();
		}
	}

}
