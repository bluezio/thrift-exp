package uk.ac.york.es.thrift.examples.streams.publishers;

import java.util.HashSet;
import java.util.Set;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.es.thrift.streams.publishers.SubscriptionPool;
import uk.ac.york.thrift.es.api.Filter;
import uk.ac.york.thrift.es.api.Notification;
import uk.ac.york.thrift.es.api.Subscriber;

public class SubscriberPool extends SubscriptionPool<Subscriber.Client, Filter> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberPool.class);
	
	public SubscriberPool() {
		super(new Subscriber.Client.Factory());
	}

	public void notifyAll(String string) {
		final Set<Subscriber.Client> badClients = new HashSet<Subscriber.Client>();
		for (Subscription sub : getSubscriptions()) {
			final Subscriber.Client client = sub.getClient();
			try {
				client.notify(new Notification(sub.getOptions().pattern + " -> " + string));
			} catch (TException ex) {
				LOGGER.error("Exception while invoking the notification", ex);
				badClients.add(client);
			}
		}
		removeSubscriptions(badClients);
	}

}
