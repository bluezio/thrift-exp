package uk.ac.york.es.thrift.examples.streams.publishers;

import java.util.Random;

import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.es.thrift.streams.publishers.RawThriftStreamPublisherTask;
import uk.ac.york.thrift.es.api.Filter;
import uk.ac.york.thrift.es.api.Publisher;

/**
 * Bidirectional RPC example, server side. Based on the example in:
 *
 *   http://joelpm.com/2009/04/03/thrift-bidirectional-async-rpc.html
 *
 * @author Antonio García-Domínguez
 */
public class PublisherExample {

	static final Logger LOGGER = LoggerFactory.getLogger(PublisherExample.class);

	private static final class PublisherHandler implements Publisher.Iface {
		private SubscriberPool pool;
		private TTransport clientTransport;

		public PublisherHandler(SubscriberPool pool, TTransport trans) {
			this.pool = pool;
			this.clientTransport = trans;
		}

		@Override
		public void subscribe(Filter f) throws TException {
			LOGGER.info("Received {}", f.pattern);
			pool.addSubscription(clientTransport, f);
		}
	}

	public static void main(String[] args) throws TTransportException {
		final SubscriberPool pool = new SubscriberPool();
		final RawThriftStreamPublisherTask task
			= new RawThriftStreamPublisherTask(2552, new TProcessorFactory(null) {
				@Override
				public TProcessor getProcessor(TTransport trans) {
					return new Publisher.Processor<Publisher.Iface>(
						new PublisherHandler(pool, trans)
					);
				}
			});
		new Thread(task).start();

		final Random rnd = new Random();
		try {
			while (true) {
				Thread.sleep(rnd.nextInt(5000));
				pool.notifyAll("I'm alive!");
			}
		} catch (InterruptedException e) {
			LOGGER.error("Interrupted main loop", e);
		}
	}
}
