package uk.ac.york.es.thrift.examples.reqresp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TSocket;

import uk.ac.york.thrift.es.api.Filter;
import uk.ac.york.thrift.es.api.Publisher;
import uk.ac.york.thrift.es.api.Publisher.Client;

/**
 * An example for a unidirectional request/response Thrift client, using
 * the Thrift compact protocol.
 *
 * @author Antonio García-Domínguez
 */
public class RequestResponseClient {

	public static void main(String[] args) throws IOException, TException {
		final TSocket tSocket = new TSocket("localhost", 2552);
		tSocket.open();
		final Client client = new Publisher.Client(new TCompactProtocol(tSocket));

		try {
			final BufferedReader rIn = new BufferedReader(
					new InputStreamReader(System.in));
			String line;
			do {
				System.out.print("Type a pattern and hit ENTER (empty line to exit): ");
				line = rIn.readLine();
				if (!"".equals(line)) {
					client.subscribe(new Filter(line));
				}
			} while (!"".equals(line));
		} finally {
			tSocket.close();
		}
	}

}
