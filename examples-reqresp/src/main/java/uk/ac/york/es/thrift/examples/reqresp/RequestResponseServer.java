package uk.ac.york.es.thrift.examples.reqresp;

import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;

import uk.ac.york.thrift.es.api.Publisher;

/**
 * A unidirectional TCP-based server for the Notification service, using
 * the Thrift compact protocol.
 *
 * @author Antonio García-Domínguez
 */
public class RequestResponseServer {
	
	private TServerSocket socket;
	private TProcessor processor;
	private TSimpleServer server;

	public RequestResponseServer(int port, TProcessor processor) throws TTransportException {
		this.socket = new TServerSocket(port);
		this.processor = processor;
	}

	public void serve() {
		server = new TSimpleServer(new Args(socket)
			.processor(processor)
			.protocolFactory(new TCompactProtocol.Factory())
		);
		server.serve();
	}

	public void stop() {
		server.stop();
	}

	public boolean isServing() {
		return server.isServing();
	}

	public static void main(String[] args) throws TTransportException {
		RequestResponseServer exampleServer = new RequestResponseServer(
				2552, new Publisher.Processor<Publisher.Iface>(new PrintlnPublisherHandler()));

		exampleServer.serve();
	}

}
