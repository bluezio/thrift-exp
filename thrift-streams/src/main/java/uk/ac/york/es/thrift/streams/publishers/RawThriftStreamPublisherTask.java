package uk.ac.york.es.thrift.streams.publishers;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple Runnable which wraps the Thrift API for starting a server that handles
 * incoming subscription requests. It's the publisher side of the bidirectional
 * TCP communication with Thrift that was shown here:
 *
 *   http://joelpm.com/2009/04/03/thrift-bidirectional-async-rpc.html
 *
 * @author Antonio Garcia-Dominguez
 */
public class RawThriftStreamPublisherTask implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(RawThriftStreamPublisherTask.class);
	private final int port;
	private TProcessorFactory processorFactory;

	public RawThriftStreamPublisherTask(int port, TProcessorFactory pool) {
		this.port = port;
		this.processorFactory = pool;
	}

	public void run() {
		try {
			final TServerSocket serverSocket = new TServerSocket(port);
			final TThreadPoolServer server = new TThreadPoolServer(
					new TThreadPoolServer.Args(serverSocket).processorFactory(processorFactory)
			);

			server.serve();
		} catch (TTransportException e) {
			LOGGER.error("Error while starting server", e);
		}
	}
}