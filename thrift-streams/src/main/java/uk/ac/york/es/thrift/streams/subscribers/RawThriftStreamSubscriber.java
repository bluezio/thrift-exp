package uk.ac.york.es.thrift.streams.subscribers;

import org.apache.thrift.TBaseProcessor;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.TServiceClientFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;

/**
 * <p>Thrift pub/sub with raw TCP, subscriber side. Based on the example in:</p>
 *
 *   http://joelpm.com/2009/04/03/thrift-bidirectional-async-rpc.html
 *
 * @author Antonio García-Domínguez
 */
public class RawThriftStreamSubscriber<IfaceClient, IfaceServer, T extends TServiceClient> {

	private final String publisherHost;
	private final int publisherPort;
	private final TServiceClientFactory<T> publisherClientFactory;
	private final Class<IfaceServer> publisherIfaceClass;

	private final TBaseProcessor<IfaceClient> subscriberProcessor;

	private MessageReceiver receiver;
	private MessageSender<T> sender;
	private IfaceServer publisherClient;

	public RawThriftStreamSubscriber(String publisherHost, int publisherPort, TServiceClientFactory<T> publisherClientFactory, Class<IfaceServer> publisherIfaceClass, TBaseProcessor<IfaceClient> subscriberProcessor) {
		this.publisherHost = publisherHost;
		this.publisherPort = publisherPort;
		this.subscriberProcessor = subscriberProcessor;
		this.publisherClientFactory = publisherClientFactory;
		this.publisherIfaceClass = publisherIfaceClass;
	}

	public RawThriftStreamSubscriber(int port, TBaseProcessor<IfaceClient> processor, TServiceClientFactory<T> clientFactory, Class<IfaceServer> serverIfaceClass) {
		this("localhost", port, clientFactory, serverIfaceClass, processor);
	}

	public synchronized IfaceServer getPublisherClient() {
		if (publisherClient == null) {
			TSocket tSocket = new TSocket(publisherHost, publisherPort);
			TBinaryProtocol protocol = new TBinaryProtocol(tSocket);
			ConnectionStatusMonitor monitor = new ConnectionStatusMonitor(protocol);

			receiver = new MessageReceiver(monitor, subscriberProcessor);
			sender = new MessageSender<T>(monitor, publisherClientFactory);

			new Thread(receiver).start();
			new Thread(sender).start();

			publisherClient = sender.getProxy(publisherIfaceClass);
		}

		return publisherClient;
	}

	public synchronized void stop() {
		if (publisherClient != null) {
			receiver.stop();
			sender.stop();
			publisherClient = null;
		}
	}

}
