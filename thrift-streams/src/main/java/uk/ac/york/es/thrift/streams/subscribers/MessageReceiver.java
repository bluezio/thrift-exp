package uk.ac.york.es.thrift.streams.subscribers;

import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Runnable that receives any incoming messages over a TCP connection and sends them
 * to the specified processor. It is quite similar to a TSimpleServer, but it uses
 * a {@link ConnectionStatusMonitor} to reestablish the connection if it drops.
 */
class MessageReceiver implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiver.class);

	private volatile boolean running;

	private final TProcessor processor;
	private final ConnectionStatusMonitor monitor;

	public MessageReceiver(ConnectionStatusMonitor monitor, TProcessor processor) {
		this.monitor = monitor;
		this.processor = processor;
	}

	public void run() {
		running = true;
		while (running) {
			TProtocol protocol = monitor.waitForConnection();
			try {
				while (processor.process(protocol, protocol) == true);
			} catch (TException e) {
				LOGGER.error("Error while receiving message: reconnecting", e);
				monitor.disconnect();
			}
		}
	}

	public boolean isRunning() {
		return running;
	}

	public void stop() {
		this.running = false;
		monitor.disconnect();
	}
}